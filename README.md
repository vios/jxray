# jxray

A python tool for CLI JSON queries and consumtion. 
Inspired by the `ConvertFrom-Json` Powershell command.

Supports
- Basic searching
- Query for a key by value
- Dump all values

More features may be implemented later.

## Example Usage:

```
curl -S https://pkgstore.datahub.io/core/country-list/data_json/data/8c458f2d15d9f2119654b29ede6e45b8/data_json.json | jxray key_query "Code" "Name=Germany"
```
